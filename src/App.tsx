import * as React from 'react';
import Channel from './Channel';
import data from './data';
import styled from 'styled-components';
import axios from 'axios';

interface AppProps {
  className?: string;
}
interface Items {
  id: number;
  title: string;
}

interface AppState {
  toggledChannel: number;
}

const Board = styled.div`
  display: flex;
  width: 500px;
  flex-flow: row wrap;
`;

class App extends React.Component<AppProps, AppState> {
  
  constructor() {
    super();
    this.channelClick = this.channelClick.bind(this);
    this.state = {toggledChannel: 1};
  }

  channelClick = async (event: React.FormEvent<HTMLElement>, id: number) => {
    try {
      const res = await axios.post(`http://10.80.253.74:3000/api/${id}`);
      console.log(res.data);
      if (res.data.success === true) {
        this.setState({ toggledChannel: id }, () => console.log(this.state.toggledChannel));
      }
    } catch (e) {
      console.log(e);
    }
  }

  createChannels = (items: Array<Items>) => {
    return items.map(this.createChannel);
  }

  createChannel = (item: Items): React.ReactElement<{}> => {
    return (
      <Channel 
        key={item.id}
        cid={item.id} 
        title={item.title}
        toggledNumber={this.state.toggledChannel}
        onClick={(e) => this.channelClick(e, item.id)}
      />
    );
  }

  render() {
    return (
        <div className={this.props.className}>
          <div className="App-header">
            <h2>Current Channel : {data.Items[this.state.toggledChannel - 1].title}</h2>  
          </div>
          <Board>
            {this.createChannels(data.Items)} 
          </Board>
        </div>
    );
  }
}

const StyledApp = styled(App)`
  margin: 20px;
`;

export default StyledApp;