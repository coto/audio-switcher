const data = {
 'Items' : [
    {
     'id': 1,
     'title': 'Channel 1',
    },
    {
     'id': 2,
     'title': 'Channel 2',
    },
    {
     'id': 3,
     'title': 'Channel 3',
    },
    {
     'id': 4,
     'title': 'Channel 4',
    },
    {
     'id': 5,
     'title': 'Channel 5',
    },
    {
     'id': 6,
     'title': 'Channel 6',
    },
    {
     'id': 7,
     'title': 'Channel 7',
    },
    {
     'id': 8,
     'title': 'Channel 8',
    },
    {
     'id': 9,
     'title': 'Channel 9',
    },
    {
     'id': 10,
     'title': 'Channel 10',
    },
    {
     'id': 11,
     'title': 'Channel 11',
    },
    {
     'id': 12,
     'title': 'Channel 12',
    },
    {
     'id': 13,
     'title': 'Channel 13',
    },
    {
     'id': 14,
     'title': 'Channel 14',
    },
    {
     'id': 15,
     'title': 'Channel 15',
    },
    {
     'id': 16,
     'title': 'Channel 16',
    }
  ]
};

export default data;