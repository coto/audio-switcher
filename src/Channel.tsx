import * as React from 'react';
import styled from 'styled-components';

interface ChannelProps {
  className?: string;
  title: string;
  cid: number;
  toggledNumber: number;
  onClick: (event: React.FormEvent<HTMLElement>, id?: number) => void;
}

class Channel extends React.Component<ChannelProps> {
  
  render() {
    return (
      <div className={this.props.className} onClick={this.props.onClick}>
        <h5>{this.props.title}</h5>
      </div>
    );
  }
}

const StyledChannel = styled(Channel)`
  display: flex;
  width: 100px;
  height: 50px;
  justify-content: center;
  align-items: center;
  text-align: center;
  border: 3px solid black;
  border-radius: 5px;
  margin: 2px;
  border-color: ${(props: ChannelProps) => props.toggledNumber === props.cid ? 'red' : 'black'};
`;

export default StyledChannel;
